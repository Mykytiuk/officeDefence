﻿using UnityEngine;
using System.Linq;
using System.Collections;

public class EnemyScript : MonoBehaviour {
	public GameObject destinationObject;
	public GameObject passPoint;
	public GameObject enemyPrefab;
	public GameObject baseCheckPoint;
	public CheckPointScript checkPointScript;

	private int health = 100;
	private Transform player;               // Reference to the player's position.
	private Transform destination;
	private NavMeshAgent nav;               // Reference to the nav mesh agent.
	private Animator anim;
	private bool onPlace = false;

	//player seek
	private float playerSeekDistance;
	private float playerSeekOffset;
	private float playerSeekMaxOffset;
	private float playerSeekMinOffset;
	private float playerSeekOffsetSign;
	private float playerSeekOffsetStep;
	private EnemyStatus status;
	private AudioSource gunFire;
    public AudioSource auch;

	private float attackDellay = 0.05f;
	private float timer = 0f;

	private float attackDuration = 15f;
	private float attackTimer = 0f;

    private float deathDelayTime = 0.5f;
    private float deathDelayValue = 0;
    private bool isDeathSoundPlayed = false;

	private enum EnemyStatus{Idle, Combat};

	void Awake () {
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		nav = GetComponent<NavMeshAgent>();
		anim = GetComponent<Animator> ();
		gunFire = GetComponent<AudioSource> ();

        //AudioSource auch = GameObject.Find("DeathAtributes").GetComponent<AudioSource>();
        //Component[] audioSourceComponents = GetComponents<AudioSource>();
        //audioSourceComponents.First(component -> )
        // gunFire = (AudioSource) audioSourceComponents[0];
        //auch = (AudioSource) audioSourceComponents.Last();
        //auch = GetComponents("");
    }

	// Use this for initialization
	void Start () {
		destination = destinationObject.transform;
		nav.SetDestination (destination.position);
		//rb = GetComponent<Rigidbody>();
		status = EnemyStatus.Idle;

		playerSeekDistance = 75f;
		playerSeekOffset = 0f;
		playerSeekMaxOffset = 5f;
		playerSeekMinOffset = -5f;
		playerSeekOffsetSign = 1f;
		playerSeekOffsetStep = 1f;
	}

	void FixedUpdate() {
		if (status == EnemyStatus.Combat) {
			attackTimer += Time.deltaTime;
			if (lookingAtPlayer ()) {
				attack ();
				attackTimer = 0;
			}
			else if(attackTimer >= attackDuration) {
				status = EnemyStatus.Idle;
				nav.Resume ();
			}
		}
		else {
			if (destination != null) {
				float dist = Vector3.Distance(destination.position, transform.position);
				onPlace = (dist <= 10);
			}
			seekPlayer ();
			anim.SetBool ("IsRunning", !onPlace);
		}

		if (onPlace) {
			rotateToPlayer ();
		}

	}

	private void seekPlayer() {
		//for (int i = -50; i <= 50; i += 5) {
		//	Vector3 rayRotation = Quaternion.AngleAxis (i + playerSeekOffset, transform.up) * transform.forward;
		//	Debug.DrawRay (transform.position, rayRotation * playerSeekDistance, Color.red);

		//	RaycastHit target;
		//	if (Physics.Raycast (transform.position, rayRotation, out target, playerSeekDistance) && target.collider.CompareTag ("Player")) {
		if (lookingAtPlayer()) {
			onPlace = true;
			nav.destination = player.position;
			destination = player;
			nav.Stop ();
			rotateToPlayer ();

			status = EnemyStatus.Combat;
		}

		//	playerSeekOffset += (playerSeekOffsetStep * playerSeekOffsetSign);
		//	playerSeekOffsetSign *= (playerSeekOffset >= playerSeekMaxOffset || playerSeekOffset <= playerSeekMinOffset) ? -1 : 1;
		//}
	}

	private bool lookingAtPlayer() {
		for (int i = -50; i <= 50; i += 5) {
			Vector3 rayRotation = Quaternion.AngleAxis (i + playerSeekOffset, transform.up) * transform.forward;
			Debug.DrawRay (transform.position, rayRotation * playerSeekDistance, Color.red);

			int layerMask = LayerMask.GetMask ("Player");
			RaycastHit target;
			if (Physics.Raycast (transform.position, rayRotation, out target, playerSeekDistance, layerMask) 
				&& target.collider.CompareTag ("Player")
			) {
				Debug.Log ("enemy see player");
				return true;
			}
			playerSeekOffset += (playerSeekOffsetStep * playerSeekOffsetSign);
			playerSeekOffsetSign *= (playerSeekOffset >= playerSeekMaxOffset || playerSeekOffset <= playerSeekMinOffset) ? -1 : 1;
		}

		return false;
	}

	private void attack() {
		timer += Time.deltaTime;
		if (timer >= attackDellay) {
			RaycastHit target;
			System.Random randomizer = new System.Random ();
			int shootMeesValue = randomizer.Next (-2, 4);
			if (Physics.Raycast (transform.position, transform.forward, out target, playerSeekDistance) && target.collider.CompareTag ("Player")) {
				//gunFire.Play ();
				if (shootMeesValue > 0) {
					print ("enemy attack player");
				}
			} else {
				//onPlace = false;
				//playerIsFound = false;
				//nav.Resume();
			}
			timer = 0;
		}
	}

	private void rotateToPlayer () {
		Vector3 lookDirection = (player.transform.position - transform.position).normalized;
		Quaternion newRotation = Quaternion.LookRotation (lookDirection);
		transform.rotation = Quaternion.Slerp (transform.rotation, newRotation, Time.deltaTime * 10);	
	}

	
	// Update is called once per frame
	void Update () {
		if (health <= 0) {
            deathDelayValue += Time.deltaTime;
            startDeath();
		}

	}

    private void startDeath()
    {
        if (!isDeathSoundPlayed)
        {
            auch.Play();
            isDeathSoundPlayed = true;
        }

        if (deathDelayValue > deathDelayTime)
        {   
            death();
            deathDelayValue = 0;
        }
    }

    private void death()
    {
        nav.enabled = false;
        GameObject.Destroy(gameObject);
        checkPointScript.spawnEnemy(destination);
    }

	public void damage(int DamageAmount) {
		health -= DamageAmount;
	}
}
