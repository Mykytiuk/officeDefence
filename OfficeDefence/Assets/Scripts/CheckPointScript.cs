﻿using UnityEngine;
using System.Collections;

public class CheckPointScript : MonoBehaviour {
	public GameObject enemyPrefab;
	public GameObject baseCheckPoint;
	public GameObject passPoint;
	public Stack enemySpawnStack = new Stack();
	public float enemySpawnDelay = 10f;
	private float timer = 0;

	void Start () {
		foreach (Transform child in transform) {
			if (!child.Equals(baseCheckPoint.transform) && child.gameObject.activeInHierarchy) {
				createEnemy(child);	
			}
		}
	}

	void Update () {
		if (enemySpawnStack.Count > 0) {
			timer += Time.deltaTime;
		}
		if (timer >= enemySpawnDelay) {
			createEnemy(enemySpawnStack.Pop() as Transform) ;	
			timer = 0;
		}
	}

	public void spawnEnemy(Transform checkPoint) {
		enemySpawnStack.Push (checkPoint);
	}

	private void createEnemy(Transform checkPoint) {
		GameObject enemy = Instantiate (enemyPrefab, baseCheckPoint.transform.position, Quaternion.LookRotation(passPoint.transform.position), GameObject.Find("Enemies").transform) as GameObject;
		EnemyScript enemyScript = enemy.GetComponent<EnemyScript> ();
		enemy.SetActive (true);
		enemyScript.destinationObject = checkPoint.gameObject;
		enemyScript.passPoint = passPoint;
		enemyScript.baseCheckPoint = baseCheckPoint;
		enemyScript.enemyPrefab = enemyPrefab;	
		enemyScript.checkPointScript = this;
	}
}
