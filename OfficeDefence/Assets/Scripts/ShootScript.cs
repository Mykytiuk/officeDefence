﻿using UnityEngine;
using System.Collections;

public class ShootScript : MonoBehaviour {
	public int DamageAmount;
	public float AllowedRange;

	private AudioSource gunFire;

	void Awake() {
		gunFire = GameObject.Find ("M9").GetComponent<AudioSource> ();
	}
	// Use this for initialization
	void Start () {
		
	}

	void Update () {
		if (Input.GetButtonDown("Fire1")) {
			gunFire.Play ();
			GameObject.Find("M9").GetComponent<Animation>().Play ("gunFire");

			Vector3 fwd = transform.TransformDirection(Vector3.forward);
			RaycastHit target;
			if (Physics.Raycast (transform.position, fwd, out target, AllowedRange)) {
				if (target.collider.CompareTag ("EnemyBody")) {
					target.transform.SendMessage ("damage", DamageAmount);
				}
				else if (target.collider.CompareTag ("EnemyHead")) {
					target.transform.SendMessage ("damage", (DamageAmount + 100));
				}
			}
		}
	}
}
