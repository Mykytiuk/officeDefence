﻿using UnityEngine;
using System.Collections;

public class CursorAnimation : MonoBehaviour {
	public GameObject topCursor;
	public GameObject bottomCursor;
	public GameObject leftCursor;
	public GameObject rightCursor;

	void Update () {
		if (Input.GetButtonDown ("Fire1")) {
			topCursor.GetComponent<Animation> ().Play ("top", PlayMode.StopAll);
			bottomCursor.GetComponent<Animation> ().Play ("bottom");
			leftCursor.GetComponent<Animation> ().Play ("left");
			rightCursor.GetComponent<Animation> ().Play ("right");
		}
	}

}
